const styleConstants = {
	COLOR: {
		PRIMARY: {
			DARK: '#2588ad',
			MAIN: '#16abd6',
			LIGHT: '#00bcd4',
		},
		SECONDARY: {
			DARK: '#83c1a6',
			MAIN: '#51c1a6',
			LIGHT: '#d0e6dc',
		},
		DEFAULT: {
			SUCCESS: '#249d3d',
			WARNING: '#ff9e37',
			DANGER: '#e1393c',
		},
		NEUTRAL: {
			LIGHT: {
				FULL: '#FFF',
			}
		}
	}
};
export default styleConstants;