import styleConstants from "../constants/styleConstants";

const stylesGitHubRepositoryItem = {
	countItem: {
		backgroundColor: styleConstants.COLOR.PRIMARY.LIGHT,
		color: styleConstants.COLOR.NEUTRAL.LIGHT.FULL,
		borderRadius: '5px',
		padding: '11px',
		margin: '5px',
	}
};
export default stylesGitHubRepositoryItem;