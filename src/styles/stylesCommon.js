import styleConstants from "../constants/styleConstants";

const stylesCommon = {
	sectionWrapper: {
		border: '2px solid',
		borderColor: styleConstants.COLOR.PRIMARY.LIGHT,
		borderRadius: '10px',
		margin: '2%',
		padding: '2%',
	},
	anchor: {
		color: styleConstants.COLOR.PRIMARY.DARK,
		textDecoration: 'none',
	},
};
export default stylesCommon;