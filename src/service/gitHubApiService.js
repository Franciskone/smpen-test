import githubApiUtil from "../util/githubApiUtil";
import gitHubApolloClient from "../gitHubApolloClient";

const gitHubApiService = {
	getUserData
};
export default gitHubApiService;

function getUserData(userLogin) {
	return gitHubApolloClient
	.query({
		query: githubApiUtil.getUserDataQuery(userLogin)
	});
}