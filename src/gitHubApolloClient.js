// import ApolloClient from "apollo-boost";
import {ApolloClient} from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from 'apollo-cache-inmemory';

import githubConstants from './constants/gitHubConstants';


// Put your GitHub TOKEN here
const TOKEN = githubConstants.TOKEN;


// Create Apollo Client
const httpLink = createHttpLink({
	uri: githubConstants.GRAPH_QL_URI
});

const authLink = setContext((_, { headers }) => {
	return {
		headers: {
			...headers,
			authorization: `Bearer ${TOKEN}`
		}
	};
});


const gitHubApolloClient = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache()
});

export default gitHubApolloClient;

