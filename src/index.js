import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import gitHubApolloClient from "./gitHubApolloClient";

ReactDOM.render(
	<ApolloProvider client={gitHubApolloClient}>
		<App />
	</ApolloProvider>
	, document.getElementById('root'));
registerServiceWorker();
