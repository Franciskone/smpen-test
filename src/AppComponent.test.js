import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import AppComponent from './AppComponent';
import GitHubUser from "./components/GitHubUser";
import GitHubRepositories from "./components/GitHubRepositories";

describe('AppComponent', () => {
 
	it('renders without crashing', () => {
		const div = document.createElement('div');
		const data = {};
		
		ReactDOM.render(<AppComponent data={data} />, div);
		ReactDOM.unmountComponentAtNode(div);
	});
	
	it('should have the `GitHubUser` "Item"', () => {
		const data = {
		  user: {
			  repositories: {
			    nodes: []
        }
      }
    };
		const wrapper = shallow(<AppComponent data={data}/> );
		
		expect(wrapper.contains(
		  <GitHubUser
			  avatarUrl={undefined}
			  bio={undefined}
			  company={undefined}
			  email={undefined}
			  name={undefined}
			  url={undefined}
			  websiteUrl={undefined}
      />
    )).toBe(true);
	});
	
	it('shouldn\'t have the `GitHubUser` "Item"', () => {
		const data = {};
		const wrapper = shallow(<AppComponent data={data}/> );
		
		expect(wrapper.contains(<GitHubUser />)).toBe(false);
	});
	
	it('should have the `GitHubRepositories` "Item"', () => {
		const data = {
			user: {
				repositories: {
					nodes: []
				}
			}
		};
		const wrapper = shallow(<AppComponent data={data}/> );
		
		expect(wrapper.contains(
		  <GitHubRepositories
			  repositoriesList={[]}
      />
    )).toBe(true);
	});
	
	it('shouldn\'t have the `GitHubRepositories` "Item"', () => {
		const data = {};
		const wrapper = shallow(<AppComponent data={data}/> );
		
		expect(wrapper.contains(<GitHubRepositories />)).toBe(false);
	});
});
