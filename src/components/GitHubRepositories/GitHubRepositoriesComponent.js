import React from 'react';
import PropTypes from 'prop-types';
import GitHubRepositoriesList from "./GitHubRepositoriesList";
import stylesCommon from "../../styles/stylesCommon";


const GitHubRepositoriesComponent = (props) => {
	
	// console.log('repositoriesList');
	// console.log(props.repositoriesList);
	
	return (
    <div className="repositories-wrapper"
			style={stylesCommon.sectionWrapper}
    >
	    <h2>REPOSITORIES</h2>
	    <GitHubRepositoriesList
        repositoriesList={props.repositoriesList}
      />
    </div>);
};

GitHubRepositoriesComponent.propTypes = {
	repositoriesList: PropTypes.array
};
GitHubRepositoriesComponent.defaultProps = {
	repositoriesList: []
};

export default GitHubRepositoriesComponent;

