import React from 'react';
import ReactDOM from 'react-dom';
import GitHubRepositoryItemComponent from './GitHubRepositoryItemComponent';

it('GitHubRepositoryItemComponent renders without crashing', () => {
  const div = document.createElement('div');
  
  ReactDOM.render(<GitHubRepositoryItemComponent />, div);
  ReactDOM.unmountComponentAtNode(div);
});
