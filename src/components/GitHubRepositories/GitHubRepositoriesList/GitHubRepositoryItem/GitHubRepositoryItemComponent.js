import React from 'react';
import PropTypes from 'prop-types';
import RepositoryItemCounter from "./RepositoryItemCounter";
import dateUtil from "../../../../util/dateUtil";


const GitHubRepositoryItemComponent = (props) => {
	const createdAt = dateUtil.dateMonthYear(props.createdAt);
	
  return (
    <div
      className="repository-item-wrapper"
      style={{
        backgroundColor: '#eee',
        borderRadius: '5px',
        padding: '4px',
        marginBottom: '2%',
      }}
    >
      <h4>{props.name.toUpperCase()}</h4>
      <div>{props.description}</div>
      <div><i>created at: {` ${createdAt}`}</i></div>
      <div style={{ margin: '30px' }}>
	      <RepositoryItemCounter label="Forks" count={props.forkCount}/>
	      <RepositoryItemCounter label="Issues" count={props.issuesCount}/>
	      <RepositoryItemCounter label="PullRequests" count={props.pullRequestsCount}/>
	      <RepositoryItemCounter label="Releases" count={props.releasesCount}/>
      </div>
    </div>);
};

GitHubRepositoryItemComponent.propTypes = {
	createdAt: PropTypes.string,
	description: PropTypes.string,
	forkCount: PropTypes.number,
	id: PropTypes.string,
	issuesCount: PropTypes.number,
	name: PropTypes.string,
	pullRequestsCount: PropTypes.number,
  releasesCount: PropTypes.number,
};
GitHubRepositoryItemComponent.defaultProps = {
	createdAt: '',
	description: '',
	forkCount: 0,
	id: '',
	issuesCount: 0,
	name: '',
	pullRequestsCount: 0,
	releasesCount: 0,
};

export default GitHubRepositoryItemComponent;

