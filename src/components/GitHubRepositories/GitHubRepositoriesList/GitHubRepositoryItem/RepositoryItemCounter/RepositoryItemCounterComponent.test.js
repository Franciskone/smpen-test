import React from 'react';
import ReactDOM from 'react-dom';
import RepositoryItemCounterComponent from './RepositoryItemCounterComponent';

it('RepositoryItemCounterComponent renders without crashing', () => {
  const div = document.createElement('div');
  
  ReactDOM.render(<RepositoryItemCounterComponent />, div);
  ReactDOM.unmountComponentAtNode(div);
});
