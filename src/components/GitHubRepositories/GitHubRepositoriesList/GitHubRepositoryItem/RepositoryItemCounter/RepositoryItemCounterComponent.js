import React from 'react';
import PropTypes from 'prop-types';
import stylesGitHubRepositoryItem from "../../../../../styles/stylesGitHubRepositoryItem";


const RepositoryItemCounterComponent = (props) => (
	<span style={stylesGitHubRepositoryItem.countItem}>
      {`${props.label}: ${props.count}`}
    </span>
);

RepositoryItemCounterComponent.propTypes = {
	label: PropTypes.string,
	count: PropTypes.number,
};
RepositoryItemCounterComponent.defaultProps = {
	label: '',
	count: 0,
};

export default RepositoryItemCounterComponent;

