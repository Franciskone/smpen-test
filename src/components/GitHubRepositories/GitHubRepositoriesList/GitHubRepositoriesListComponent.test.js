import React from 'react';
import ReactDOM from 'react-dom';

import GitHubRepositoriesListComponent from './GitHubRepositoriesListComponent';

describe('GitHubUserComponent', () => {
	
  it('GitHubRepositoriesListComponent renders without crashing', () => {
		const div = document.createElement('div');
		
		ReactDOM.render(<GitHubRepositoriesListComponent />, div);
		ReactDOM.unmountComponentAtNode(div);
	});
 
});
