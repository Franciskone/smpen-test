import React from 'react';
import PropTypes from 'prop-types';

import GitHubRepositoryItem from "./GitHubRepositoryItem";


const GitHubRepositoriesListComponent = (props) => {
 
	const listItem = (data) => <GitHubRepositoryItem
		key={data.id}
		createdAt={data.createdAt}
		description={data.description}
		forkCount={data.forkCount}
		id={data.id}
		issuesCount={data.issues.totalCount}
		name={data.name}
		pullRequestsCount={data.pullRequests.totalCount}
		releasesCount={data.releases.totalCount}
	/>;
		
  return (
    <div className="repositories-list-wrapper">
	    {props.repositoriesList.map(listItem)}
    </div>);
};

GitHubRepositoriesListComponent.propTypes = {
	repositoriesList: PropTypes.array
};
GitHubRepositoriesListComponent.defaultProps = {
	repositoriesList: []
};

export default GitHubRepositoriesListComponent;

