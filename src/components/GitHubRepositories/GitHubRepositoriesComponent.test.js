import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import GitHubRepositoriesComponent from './GitHubRepositoriesComponent';
import GitHubRepositoriesList from "./GitHubRepositoriesList";

describe('GitHubUserComponent', () => {
	
  it('GitHubRepositoriesComponent renders without crashing', () => {
		const div = document.createElement('div');
		
		ReactDOM.render(<GitHubRepositoriesComponent/>, div);
		ReactDOM.unmountComponentAtNode(div);
	});
  
  it('should have "GitHubRepositoriesList"', () => {
	  const wrapper = shallow(<GitHubRepositoriesComponent /> );
	
	  expect(wrapper.contains(<GitHubRepositoriesList />)).toBe(true);
  });
});
