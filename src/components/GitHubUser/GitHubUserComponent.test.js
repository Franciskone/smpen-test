import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import GitHubUserComponent from './GitHubUserComponent';

describe('GitHubUserComponent', () => {
	
  it('GitHubUserComponent renders without crashing', () => {
		const div = document.createElement('div');
		
		ReactDOM.render(<GitHubUserComponent/>, div);
		ReactDOM.unmountComponentAtNode(div);
	});
	
  it('should have "bio paragraph"', () => {
	  const bioText = 'bio text';
	  const wrapper = shallow(<GitHubUserComponent bio={bioText} /> );
	
	  expect(wrapper.contains(<p>{bioText}</p>)).toBe(true);
  });
});