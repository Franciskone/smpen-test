import React from 'react';
import PropTypes from 'prop-types';
import stylesCommon from "../../styles/stylesCommon";


const GitHubUserComponent = (props) => {
  return (
  	<div className="user-wrapper"
	    style={stylesCommon.sectionWrapper}
	  >
	    <h2>
	      <a href={props.url}
	        style={stylesCommon.anchor}
	      >
	        {props.name}
	      </a>
	    </h2>
		  
	    <img src={props.avatarUrl}
	      alt=''
	      style={{
	        borderRadius: '100%',
		      width: '20%',
		      height: '20%',
	      }}
	    />
	    
	    <div className="user-bio">
		    <p>{props.bio}</p>
	    </div>
	    
	    <div className="user-info">
		    <h3>INFO</h3>
		    <ul>
			    <li>email: {props.email}</li>
			    <li>company: {props.company}</li>
	        <li>{'website: '}
	          <a href={props.websiteUrl}
							target="_blank"
              style={stylesCommon.anchor}
	          >
		          {props.websiteUrl}
	          </a>
	        </li>
		    </ul>
	    </div>
	  </div>
  );
};

GitHubUserComponent.propTypes = {
	avatarUrl: PropTypes.string,
  bio: PropTypes.string,
  company: PropTypes.string,
  email: PropTypes.string,
	name: PropTypes.string,
	url: PropTypes.string,
	websiteUrl: PropTypes.string
};
GitHubUserComponent.defaultProps = {
	avatarUrl: '',
	bio: '',
	company: '',
	email: '',
	name: '',
	url: '',
	websiteUrl: ''
};

export default GitHubUserComponent;