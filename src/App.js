import { graphql } from 'react-apollo';

import githubApiUtil from "./util/githubApiUtil";
import AppComponent from "./AppComponent";


const AppWithData = graphql(githubApiUtil.getUserDataQuery('gaearon'))(AppComponent);
export default AppWithData;

