import React, {Component} from "react";
import logo from './logo.svg';

import GitHubUser from "./components/GitHubUser";
import GitHubRepositories from "./components/GitHubRepositories";
import './App.css';


class AppComponent extends Component {
	render() {
		const userData = this.props.data.user || null;
		const repositoriesList = this.props.data.user ? this.props.data.user.repositories.nodes : null;
		
		return (
			<div className="App">
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h1 className="App-title">Welcome to Francesco Cappelli Test</h1>
				</header>
				<div
					className="App-body"
					style={{
						padding: '3%',
					}}
				>
					{ !!userData
						? <GitHubUser
							avatarUrl={userData.avatarUrl}
							bio={userData.bio}
							company={userData.company}
							email={userData.email}
							name={userData.name}
							url={userData.url}
							websiteUrl={userData.websiteUrl}
						/>
						: null
					}
					{ !!repositoriesList
						? <GitHubRepositories
								repositoriesList={repositoriesList}
							/>
						: null
					}
				</div>
			</div>
		);
	}
}

export default AppComponent;