import dateUtil from "./dateUtil";

it('formats date  correctly', () => {
	const rawDate = '2018-02-26T19:40:11Z';
	const formattedDate = '02/26/2018';
	
	expect(dateUtil.dateMonthYear(rawDate)).toBe(formattedDate);
});
