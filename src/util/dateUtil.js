import moment from "moment";

const dateUtil = {
	dateMonthYear
};
export default dateUtil;

function dateMonthYear(rawTime) {
	return moment(rawTime).format('L');
}