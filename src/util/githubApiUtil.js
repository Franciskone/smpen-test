import gql from "graphql-tag";

const githubApiUtil = {
	getUserDataQuery
};
export default githubApiUtil;

function getUserDataQuery(userLogin) {
	return gql`
	  query {
		  user(login: ${userLogin}) {
		    id
		    avatarUrl
		    bio
		    company
		    email
		    name
		    url
		    websiteUrl
				repositories(first: 10) {
		      nodes {
		        id
		        name
		        createdAt
		        description
		        forkCount
		        issues {
		          totalCount
		        }
		        pullRequests {
		          totalCount
		        }
		        releases {
		          totalCount
		        }
		      }
		    }
		  }
		}
	`;
}